# Gonçalo Nascimento Silva, al60743
# Pedro Carvalho Martins, al76934
# Paulo Moreira, al77289

import random
from statistics import mean, median, variance
from tokenize import String

#Exercicio 1
#a)

randomlist = []


def geralista(num):
    
    for i in range(0,num):
        n = random.randint(1,5)
        randomlist.append(n)

    return randomlist

num = (int) (input('Qual o tamanho da lista?'))
print("Lista : " + str(geralista(num)))

#b)
print( "Menor: " +  str(min(randomlist)))

#c)
print("Média : " + str(mean(randomlist)))

#d)
print("Mediana: " + str(median(randomlist)))

#e)
print("Variancia: " + str(variance(randomlist)))

